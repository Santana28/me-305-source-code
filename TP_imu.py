"""
    @file           TP_imu.py
    @brief          Driver class that sets up and receives data from an IMU.
"""

from pyb import I2C
import struct

class TP_IMU:
    ''' @brief      An IMU driver class 
        @details    Objects of this class can be used to recieve IMU data.
    '''
    ## Constructor
    def __init__(self):
        ''' @brief      Initializes and returns an IMU object 
            @details    Initializes the IMU in master mode, allowing the 
                        retrieval of data.
        '''
        self.i2c = I2C(1, I2C.MASTER)
        
    ## Method to change operaring mode of IMU
    def op_mode(self, register):
        ''' @brief          Method to change operating mode of IMU.
            @param register Register address of operating mode of IMU.
        '''
        self.i2c.mem_write(register, 0x28, 0x3D)
        
    ## Method to retrieve calibration status from IMU
    def cal_status(self):
        ''' @brief     Method to retrieve calibration status from IMU 
            
        '''
        return self.i2c.mem_read(1, 0x28, 0x35)
        
    ## Method to retrieve calibration coefficients from IMU
    def cal_coeff(self):
        ''' @brief   Method to retrieve calibration coefficients from IMU
            @details
        '''
        return self.i2c.mem_read(22, 0x28, 0x55)
    
    ## Method to write calibration coefficients to the IMU from pre-recorded data
    def set_cal(self, coeff):
        ''' @brief        Method to write calibration coefficients to the IMU from pre-recorded data
            @param coeff  Calibration coefficients to write to IMU.
        '''
        self.i2c.mem_write(coeff, 0x28, 0x1A)
        
    ## Method to read Euler angles from the IMU to use as state measurements
    def eul(self):
        ''' @brief Method to read Euler angles from the IMU to use as state measurements
            
        '''
        self.eul_signed_ints = struct.unpack('<hhh', self.i2c.mem_read(6, 0x28, 0x1A))
        self.eul_vals = tuple(self.eul_int/16 for self.eul_int in self.eul_signed_ints)
        return self.eul_vals
    
    ## Method to read angular velocity from the IMU to use as  state measurements
    def omega(self):
        ''' @brief Method to read angular velocity from the IMU to use as  state measurements
        '''
        self.omega_signed_ints = struct.unpack('<hhh', self.i2c.mem_read(6, 0x28, 0x14))
        self.omega_vals = tuple(self.omega_int/16 for self.omega_int in self.omega_signed_ints)
        return self.omega_vals
    
