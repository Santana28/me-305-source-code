'''
@file fibonaci.py
'''

def fib(idx):
    
    ''' 
    @brief     This function calculates a Fibonacci number at a specific index index.
    @param idx An integer specifying the index of the desired Fibonacci number
    '''
    
    my_list = [0, 1]
    f0 = 0
    f1 = 1
    if idx == 0:
        return '0'
    elif idx == 1:
        return '1'
    for n in range(2,idx+1):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
        my_list.append(f2)
        n += 1
    return f2

if __name__ == '__main__':
    
    while True:
        try:
            
            ## The index of the Fibonacci number
            my_int = input('Enter a positive whole number or type quit to exit: ' )
            
            ## This if statement is used to close the program
            if my_int == 'quit':
                break
            else:
                
                ## The user's input is converted to an integer
                idx = int(my_int)
        
        ## This except statement accounts for inputs that are not whole numbers
        except ValueError:
                print('The number you entered is not a whole number, please enter a positive whole number or type quit to exit: ')
        else:
            
            ## This if statement accounts for inputs that are negative
            if idx<0:
                print('The number you entered is negative, please enter a positive whole number or type quit to exit: ')
            else:
                print('The Fibonacci number at ''index {:} is {:}'.format(idx, fib(idx)))
                
                
                
                