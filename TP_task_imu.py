"""
    @file           TP_task_imu.py
    @brief          Task that implements IMU control and updates data information.        
"""
import TP_imu
import time
import os

class TP_Task_IMU:
    ''' @brief      IMU Task Class
        @details    This task continuously updates data from the IMU onto shared
                    variables using the IMU driver class.
    '''

    def __init__(self, period, theta_x_share, theta_y_share, omega_x_share, omega_y_share):
        ''' @brief                  Constructs an IMU task
            @details                The IMU task continuously updates the anglur positon
                                    of the platform using functions from the IMU driver
            @param period           The perios, in microseconds, between runs of the task
            @param theta_x_share    A shares.Share object representing the angular position
                                    position of the platform with respect to the x axis
            @param theta_y_share    A shares.Share object representing the angular position
                                    of the platform with respect to the y axis
            @param omega_x_share    A shares.Share object representing the angular velocity
                                    of the platfrom with respect to the x axis
            @param omega_y_share    A shares.Share object representing the angular velocity 
                                    of the platform with respect to the y axis
        '''
        ## The period (in us) of the task
        self.period = period
        
        ## A shares.Share object that represents the angular position of the
        ## platform with respect to the x axis
        self.theta_x_share = theta_x_share
        
        ## A shares.Share object that represents the angular position of the
        ## platform with respect to the y axis
        self.theta_y_share = theta_y_share
        
        ## A shares.Share object that represents the angular velocity of the
        ## platform with respect to the x axis
        self.omega_x_share = omega_x_share
        
        ## A shares.Share object that represents the angular velocity of the
        ## platform with respect to the y axis
        self.omega_y_share = omega_y_share
        
        ## A TP_imu.TP_IMU object that is used to run IMU functions in the task
        self.IMU = TP_imu.TP_IMU()
        
        ## The time to run the next iteration of the task
        self.next_time = time.ticks_add(time.ticks_us(), self.period)
        
        ## The number of runs of the state machine
        self.runs = 0
        
        ## The configuration operating mode of the IMU
        self.config = 0b0000
        
        ## The NDOF operating mode of the IMU 
        self.NDOF = 0b1100
        
        ## This if statement is responsible for importing the calibration 
        ## coefficients from a file that should be located in pybeflash
        if 'IMU_cal_coeffs' in os.listdir():
            print('Import Calibration Coefficients...')
            self.IMU.op_mode(self.config)
            with open('CalibrationCoefficients' , 'r') as f:
                self.IMU.set_cal(f.read())
            self.IMU.op_mode(self.NDOF)
        else:
            self.IMU.op_mode(self.NDOF)
            calstat = self.IMU.cal_status()[0]
            print('calibrating')
            mag_calib = False
            acc_calib = False
            gyr_calib = False
            while calstat != 0b11111111:
                if ((calstat & 0b11) == 0b11) & (mag_calib == False):
                    print('magnetometer calibrated')
                    mag_calib = True
                if ((calstat & 0b1100) == 0b1100) & (acc_calib == False):
                    print('accelerometer calibrated')
                    acc_calib = True
                if ((calstat & 0b110000) == 0b110000) & (gyr_calib == False):
                    print('gyroscope calibrated')
                    gyr_calib = True
                calstat = self.IMU.cal_status()[0]
            print('calibrated')
            
            cal = self.IMU.cal_coeff()
            print(cal)
            cal_list = open('CalibrationCoefficients', 'w')
            cal_list.write(cal)
            cal_list.close()
            
    def run(self):
        ''' @brief      Runs one iteration of the FSM
            @details    Continuously updates the angular position and velocities of
                        the platform using functions from the IMU driver
        '''
        if(time.ticks_us() >= self.next_time):
            
            self.angles = self.IMU.eul()
            self.theta_x_share.write(self.angles[2])
            self.theta_y_share.write(self.angles[1])
            self.ang_vel = self.IMU.omega()
            self.omega_x_share.write(self.ang_vel[2])
            self.omega_y_share.write(self.ang_vel[1])
            self.next_time += self.period
            self.runs += 1
        
        
            self.next_time += self.period
            self.runs += 1


                    

