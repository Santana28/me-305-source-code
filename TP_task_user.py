"""
    @file           TP_task_user.py
    @brief          Task that implements an FSM for a user interface. 
            
"""
import pyb
import utime

##Initialization state
s0 = 0

##User input state
s1 = 1

class TP_Task_User:
    ''' @brief      User Interface Task class
        @details    This task is designed as an FSM and receives user input to 
                    perform various actions.
    '''
    
    def __init__(self, period, con_flag, dat_flag):
        ''' @brief              Constructs A user interface task
            @details            The user interface task is implimented as a finite
                                state machine
            @param period       The period, in microseconds, between runs of the task
            @param con_flag     A shares.Share object indicating when to begin the controller task
            @param dat_flag     A shares.Share object indicating when to begin data collection
        '''
        
        ## The period (in us) of the task
        self.period = period
        
        ## A shares.share object that indicates when to begin the controller task
        self.con_flag = con_flag
        
        
        ## A shares.Share object that signals when to begin datat collection
        self.dat_flag = dat_flag
        
        ## The number of runs of the state machine
        self.runs = 0
        
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()    
        
        ## The initialization state of the FSM
        self.state = s0
        
        ## The time to run the next iteration of the task
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)       
        
    def run(self):
        ''' @brief      Runs one iteration of the FSM
            @details    Takes user input to begin the balancing protocol or begin
                        data collection
        '''
        
        self.instruct = """
        ------------------------------------------------------------------------
        Use the following inputs to perform their correlated actions           |
        ------------------------------------------------------------------------
        |b| Begin balancing ball                                               |
        |d| Begin data collection                                              |
        |s| Stop data collection                                               |
        ------------------------------------------------------------------------                                                                    
        """    
        current_time =  utime.ticks_us()  
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            
            if self.state == s0:
                print(self.instruct) 
                self.state = s1
            
            elif self.state == s1:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()                  
                    
                    if self.char_in == 'b':
                        self.con_flag.write(1)
                        print('Beginning Balancing Protocol')
                        
                    elif self.char_in == 'd':
                        self.dat_flag.write(1)
                        print('Beginning Data Collection')
                        
                    elif self.char_in == 's':
                        self.dat_flag.write(2)
                     
                       
                pass
        
            else:
                raise ValueError('Invalid State')
                    
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1
                
