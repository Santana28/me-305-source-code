
"""
@file       main.py
@brief      Runs tasks that creates Encoder Interface
@details    Runs task_encoder.py and task_user.py to set up the user interface and update encoder position.
@author     Cesar Santana
@author     Dylan Ruiz
@date       10/21/2021
"""

import task_user
import task_encoder
import task_motor
import encoder
import shares

def main():
    ''' @brief      The main program runs both Tasks
        @details    Main runs both task_encoder and task_user in a while loop
    '''
    

    ## A shares.Share object used by the tasks for sharing the encoder position    
    pos1_share = shares.Share(0)
    pos2_share = shares.Share(0)
    
    ## A shares.Share object used by the tasks for sharing the change in encoder position
    delta1_share = shares.Share(0)
    delta2_share = shares.Share(0)
    
    ## A shares.share object used by the tasks for sharing the desired duty cycle
    duty_share = shares.Share(0)
    
    ## A shares.Share object representing the refernece velocity for the motors
    omega_share = shares.Share(0)
    ## A shares.Share object representing the measured velocity of motor 1
    omegameas1_share = shares.Share(0)
    ## A shares.Share object representing the measured velocity of motor 2
    omegameas2_share = shares.Share(0)
    
    ## A shares.Share object representing the proportional gain of the closed loop controller
    Pgain_share = shares.Share(0)
    
    ## A shares.Share object used by the tasks for sharing the change in a flag variable used to run different encoder functions
    enc_flag = shares.Share(0)
    ## A shares.share object used by the tasks for sharing the change in a flag variable used to run differen motor functions
    mot_flag = shares.Share(0)
    
    ## initializes encoder.py
    encoder.Encoder()
    
    ## A task.encoder.Task_Encoder object used to run the encoder task continuously  
    task1 = task_encoder.Task_Encoder(5_000, pos1_share, pos2_share, delta1_share, delta2_share, omegameas1_share, omegameas2_share, enc_flag)
    
    ##  A task.encoder.Task_Encoder object used to run the encoder task continuously 
    task2 = task_user.Task_User(5_000, pos1_share, pos2_share, delta1_share, delta2_share, omega_share, omegameas1_share, omegameas2_share, duty_share, Pgain_share, enc_flag, mot_flag)
    
    ## A task.motor.Task_Motor object used to run the mototr task continuously
    task3 = task_motor.Task_Motor(5_000, duty_share, omega_share, omegameas1_share, omegameas2_share, Pgain_share, mot_flag)
    
    ## task list used to run in while loop
    task_list = [task1, task2, task3]
    
    while(True):
        try: 
            for task in task_list:
                task.run()
        except KeyboardInterrupt:
            break
            print('Program Terminating')
    
if __name__ =='__main__':
   main()

