'''
    @file       task_user.py
    @brief      User interface task for cooperative multitasking of encoder 
    @details    Implements a finite state machine that performs different actions 
                based on character input.
'''

import utime
import pyb

## Represents state 0, initialization state
s0_init = 0
## Represents state 1, key command state
s1 = 1
## Represents state 2, data collection for encoder 1
s2 = 2
## Represents state 3, stop command fordata collection of encoder 1
s3 = 3
## Represents state 4, duty cycle input for motor 1
s4 = 4
## Represents state 5, duty cycle for motor 2
s5 = 5
## Represents state 6, data collenction for encoder 2
s6 = 6
## Represents state 7, stop command for data collection of encoder 2
s7 = 7
## Represents state 8, proportional gain input for motor 1
s8 = 8
## Represents state 9, velocity setpoint for motor 1
s9 = 9
## Represents state 10, step response data collection for motor 1
s10 = 10
## Represesnts state 11, stop command for data collection of motor 1
s11 = 11
## Represents state 12, proportional gain input for motor 2
s12 = 12
## Represents staee 13, velocity setpoint for motor 2
s13 = 13
## Represents state 14, step response data collection for motor 2
s14 = 14
## Represents state 15, stop command for data collection of motor 2
s15 = 15

class Task_User:
    '''@brief       User interface task for encoder project.
       @details     Implements a finite state machine.
    '''

    def __init__(self, period, pos1_share, pos2_share, delta1_share, delta2_share, omega_share, omegameas1_share, omegameas2_share, duty, Pgain_share, enc_flag, mot_flag):
        ''' @brief              Contructs a user interface task.
            @details            The user interface task is implemented as a finite state machine.
            @param period       The period, in microseconds, between runs of the task.
            @param pos_share    A shares.Share object used to hold the position of the encoder. 
            @param duty_share   A shares.Share object used to set the duty cycle of the motor
            @param delta_share  A shares.Share object used to hold the change of position of the encoder.
            @param enc_flag     A shares.Share object used to indicate which encoder function to run.
            @param mot_flag     A shares.Share object used to indicate which motor function to run
        '''
        ## A shares.Share object representing the position of encoder 1
        self.pos1_share = pos1_share
        ## A shares.Share object representing the position of encoder 2
        self.pos2_share = pos2_share
        
        ## A shares.Share object representing the change in position of encoder 1
        self.delta1_share = delta1_share
        ## A shares.Share object representing the change in position of encoder 2
        self.delta2_share = delta2_share
        
        ## A shares.Share object representing the desired duty cycle for the motors
        self.duty_share = duty
        
        ## A shares.Share object representing the angular speed of the encoder
        self.omega_share = omega_share
        
        ## A shares.Share object representin the measured velocity of motor 1
        self.omegameas1_share = omegameas1_share
        ## A shares.share object representing the measured velocity of motor 2
        self.omegameas2_share = omegameas2_share
        
        ## A shares.Share object representing the proportional gain of the controller
        self.Pgain_share = Pgain_share
        
        ## A shares.Share object representing the indicator for which encoder function to run
        self.enc_flag = enc_flag

        ## A shares.Share object representing the indicator for which motor function to run
        self.mot_flag = mot_flag
        
        ## The state to run on the next iteration of the finite state machine
        self.state = s0_init
        
        ## The string that will be used to get user input for the duty cycle
        self.my_str = ''
        
        ## The period (in us) of the task 
        self.period = period
        
        ## The number of runs of the state machine
        self.runs = 0
        
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        
        ## The utime to run the next iteration of the task
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        ## Starts the enc flag at state 0
        self.enc_flag.write(0)
        
        ## Variable used to index through lists
        self.idx = 0
        
        ## List used to display encoder position while collecting data
        self.my_list_pos = []
        
        ## List used to display time while collecting data
        self.my_list_time = []
        
        ## List usaed to display the pwm for every iteration
        self.my_list_actuation = []
        
        ## The period used to collect data during state 2
        self.t_period = 100
        
        ## The time to run the next iteration state 2
        self.next_time2= utime.ticks_add(utime.ticks_ms(),self.t_period)
        
        ## The reference time when entering state 2
        self.Tref = 0
        
        
    def run(self):
        ''' @brief      Runs one iteration of the FSM
        '''
        
        self.instruct = """
        ------------------------------------------------------------------------
        Use the following inputs to perform their correlated actions           |
        ------------------------------------------------------------------------
        |z|Zero the position of encoder 1                                      |
        |Z|Zero the positon of encoder 2                                       |
        |p|Print out the position of encoder 1                                 |
        |P|Print the position of encoder 2                                     |
        |d|Print out the delta for encoder 1                                   |
        |D|Print out the delta for encoder 2                                   |
        |m|Enter duty cycle for motor 1                                        |
        |M|Enter duty cycle for motor 2                                        |
        |c|Clear fault condition                                               |
        |g|Collect encoder 1 data for 30 sec and print [position,time] list    |
        |G|Collect encoder 2 data for 30 sec and print [positon,time] list     |
        |s|End data collection prematurely for encoder 1                       |
        |S| End data collection prematurely for encoder 2                      |
        ________________________________________________________________________
        |1| Perform a step response for motor 1                                |
        |2| Perform a step response for motor 2                                |
        |s| End data collection prematurely                                    |
        ------------------------------------------------------------------------                                                                    
        """
        current_time =  utime.ticks_us()  
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
          
            if self.state == s0_init:
                print(self.instruct) 
                self.state = s1
                
            elif self.state == s1:
                self.enc_flag.write(0)
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()                  
                    
                    if self.char_in == 'z':
                        self.enc_flag.write(1)
                        print('Encoder 1 Position Zeroed')         
                    elif self.char_in == 'Z':
                        self.enc_flag.write(3)
                        print('Encoder 2 Position Zeroed')
                                                
                    elif self.char_in == 'p': 
                        self.enc_flag.write(5)
                    elif self.char_in == 'P':
                        self.enc_flag.write(6)                     
                        
                    elif self.char_in == 'd':
                        self.enc_flag.write(2)
                    elif self.char_in == 'D':
                        self.enc_flag.write(4)                 
                    
                    elif self.char_in == 'm':
                        self.state = s4
                        print('Enter Duty Cycle For Motor 1')
                    elif self.char_in == 'M':
                        self.state = s5
                        print('Enter Duty Cycle For Motor 2')               
                    
                    elif self.char_in == 'c' or self.char_in == 'C':
                        self.mot_flag.write(3)
                        print('Fault Cleared')                     
                        
                    elif self.char_in == 'g':
                        self.state = s2
                        self.Tref = utime.ticks_ms()
                        self.end_30 = utime.ticks_add(self.Tref, 30000)
                        print('Collecting Data on encoder 1....')
                    elif self.char_in == 'G':
                        self.state = s6
                        self.Tref = utime.ticks_ms()
                        self.end_30 = utime.ticks_add(self.Tref, 30000)
                        print('Collecting Data on encoder 2....')
                    
                    elif self.char_in == '1':
                        self.state = s8
                        print('Enter a Proportional Gain Value')
                    elif self.char_in == '2':
                        self.state = s12
                            
                    elif self.char_in == 'h' or self.char_in == 'H':
                        print(self.instruct) 
                    else:
                        print('Command \'{:}\' is invalid.'.format(self.char_in))
                        pass
                       
                    
            elif self.state == s2:
                self.enc_flag.write(0)
                self.idx = 0
                
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 's':
                        self.state = s3
                        
                if (utime.ticks_ms() >= self.end_30):
                    self.state = s3
                    
                if (utime.ticks_ms() >= self.next_time2):
                    self.pos1_share.read()
                    self.my_list_pos.append(self.pos1_share.read())
                    self.my_list_time.append(utime.ticks_diff(utime.ticks_ms(),self.Tref))
                    self.next_time2= utime.ticks_add(utime.ticks_ms(),self.t_period)
                            
                     
            elif self.state == s3:
                for idx in range(len(self.my_list_pos)):
                            print('[{:},{:}]'.format(self.my_list_pos[idx],
                                  self.my_list_time[idx]/1000))
                            
                self.state = s1
                self.my_list_pos = []
                self.my_list_time = []          
            
            
            elif self.state == s4:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    
                    if self.char_in.isdigit() == True:
                         self.my_str += self.char_in
                         self.ser.write(self.char_in)
                         
                    #elif self.char_in.isdigit() == False:
                    if self.char_in == '-':
                        if self.my_str == '':
                            self.my_str += self.char_in 
                            self.ser.write(self.char_in)
                             
                    elif self.char_in == '\x7F':
                        if self.my_str == '':
                            self.my_str = self.my_str
                            self.ser.write(self.char_in)
                             
                        else:
                            a = len(self.my_str)
                            b = a-1
                            self.my_str = self.my_str[:b]
                            self.ser.write(self.char_in)
                             
                    elif self.char_in == '\r' or self.char_in == '\n':
                         self.duty_share.write(int(self.my_str))
                         self.mot_flag.write(1)
                         self.state = s1
                         self.my_str = ''
                         
            elif self.state == s5:
                    if self.ser.any():
                        self.char_in = self.ser.read(1).decode()
                        
                        if self.char_in.isdigit() == True:
                             self.my_str += self.char_in
                             self.ser.write(self.char_in)
                             
                        if self.char_in == '-':
                            if self.my_str == '':
                                self.my_str += self.char_in 
                                self.ser.write(self.char_in)
                                 
                        elif self.char_in == '\x7F':
                            if self.my_str == '':
                                self.my_str = self.my_str
                                self.ser.write(self.char_in)
                                 
                            else:
                                a = len(self.my_str)
                                b = a-1
                                self.my_str = self.my_str[:b]
                                self.ser.write(self.char_in)
                                 
                        elif self.char_in == '\r' or self.char_in == '\n':
                             self.duty_share.write(int(self.my_str))
                             self.mot_flag.write(2)
                             self.state = s1
                             self.my_str = ''
                             
            elif self.state == s6:
                self.enc_flag.write(0)
                self.idx = 0
                
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 'S':
                        self.state = s7
                        
                if (utime.ticks_ms() >= self.end_30):
                    self.state = s7
                    
                if (utime.ticks_ms() >= self.next_time2):
                    self.pos2_share.read()
                    self.my_list_pos.append(self.pos2_share.read())
                    self.my_list_time.append(utime.ticks_diff(utime.ticks_ms(),self.Tref))
                    self.next_time2= utime.ticks_add(utime.ticks_ms(),self.t_period)
                            
                    
            elif self.state == s7:
                for idx in range(len(self.my_list_pos)):
                            print('[{:},{:}]'.format(self.my_list_pos[idx],
                                  self.my_list_time[idx]/1000))
                            
                self.state = s1
                self.my_list_pos = []
                self.my_list_time = []   


            elif self.state == s8:
                 # print('Enter a Proportional Gain Value')
                 if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    
                    if self.char_in.isdigit() == True:
                         self.my_str += self.char_in
                         self.ser.write(self.char_in)
                         
                    #elif self.char_in.isdigit() == False:
                    if self.char_in == '-':
                        if self.my_str == '':
                            self.my_str += self.char_in 
                            self.ser.write(self.char_in)
                             
                    elif self.char_in == '\x7F':
                        if self.my_str == '':
                            self.my_str = self.my_str
                            self.ser.write(self.char_in)
                             
                        else:
                            a = len(self.my_str)
                            b = a-1
                            self.my_str = self.my_str[:b]
                            self.ser.write(self.char_in)
                             
                    elif self.char_in == '\r' or self.char_in == '\n':
                         self.Pgain_share.write(int(self.my_str))
                         self.mot_flag.write(6)
                         self.state = s9
                         print('Enter a Velocity Setpoint')
                         self.my_str = ''
                         
                         
            elif self.state == s9:
                 # print('Enter a Velocity Setpoint')
                 if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    
                    if self.char_in.isdigit() == True:
                         self.my_str += self.char_in
                         self.ser.write(self.char_in)
                         
                    if self.char_in == '-':
                        if self.my_str == '':
                            self.my_str += self.char_in 
                            self.ser.write(self.char_in)
                             
                    elif self.char_in == '\x7F':
                        if self.my_str == '':
                            self.my_str = self.my_str
                            self.ser.write(self.char_in)
                             
                        else:
                            a = len(self.my_str)
                            b = a-1
                            self.my_str = self.my_str[:b]
                            self.ser.write(self.char_in)
                             
                    elif self.char_in == '\r' or self.char_in == '\n':
                         self.omega_share.write(int(self.my_str))
                         self.enc_flag.write(0)
                         self.state = s10
                         self.my_str = ''
                         self.Tref = utime.ticks_ms()
                         self.end_10 = utime.ticks_add(self.Tref, 10000)
                         self.end_start = utime.ticks_add(self.Tref,10)
                         print('Running Motor 1 for 10 Seconds')
                         self.mot_flag.write(4)
            
            
            elif self.state == s10:
                
                self.idx = 0
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 's' or self.char_in == 'S':
                        self.state = s11
                        self.duty_share.write(0)
                        self.mot_flag.write(1)
                        self.omega_share.write(0)
                if (utime.ticks_ms() >= self.end_start):
                    if self.delta1_share.read() == 0:
                        self.state = s11
                        self.duty_share.write(0)
                        self.mot_flag.write(1)
                        self.omega_share.write(0)
                        print('Fault detected, program stopped')
                if (utime.ticks_ms() >= self.end_10):
                    self.state = s11
                    self.duty_share.write(0)
                    self.mot_flag.write(1)
                    self.omega_share.write(0)
                if(utime.ticks_ms() >= self.next_time2):
                    self.my_list_pos.append(self.omegameas1_share.read())
                    self.my_list_time.append(utime.ticks_diff(utime.ticks_ms(),self.Tref))
                    self.my_list_actuation.append(self.duty_share.read())
                    self.next_time2= utime.ticks_add(utime.ticks_ms(),self.t_period)
                
                
            elif self.state == s11:
                for idx in range(len(self.my_list_pos)):
                            print('{:}, {:}, {:}'.format(self.my_list_pos[idx],
                                  self.my_list_time[idx]/1000,self.my_list_actuation[idx]))
                self.state = s1
                self.my_list_pos = []
                self.my_list_time = [] 
                self.my_list_actuation = []
                self.duty_share.write(1)
                self.mot_flag.write(1)
            
            
            elif self.state == s12:
                 print('Enter a Proportional Gain Value')
                 if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    
                    if self.char_in.isdigit() == True:
                         self.my_str += self.char_in
                         self.ser.write(self.char_in)
                         
                    #elif self.char_in.isdigit() == False:
                    if self.char_in == '-':
                        if self.my_str == '':
                            self.my_str += self.char_in 
                            self.ser.write(self.char_in)
                             
                    elif self.char_in == '\x7F':
                        if self.my_str == '':
                            self.my_str = self.my_str
                            self.ser.write(self.char_in)
                             
                        else:
                            a = len(self.my_str)
                            b = a-1
                            self.my_str = self.my_str[:b]
                            self.ser.write(self.char_in)
                             
                    elif self.char_in == '\r' or self.char_in == '\n':
                         self.Pgain_share.write(int(self.my_str))
                         self.mot_flag.write(6)
                         self.state = s13
                         self.my_str = ''
                        
                         
            elif self.state == s13:
                 print('Enter a Velocity Setpoint')
                 if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    
                    if self.char_in.isdigit() == True:
                         self.my_str += self.char_in
                         self.ser.write(self.char_in)
                         
                    #elif self.char_in.isdigit() == False:
                    if self.char_in == '-':
                        if self.my_str == '':
                            self.my_str += self.char_in 
                            self.ser.write(self.char_in)
                             
                    elif self.char_in == '\x7F':
                        if self.my_str == '':
                            self.my_str = self.my_str
                            self.ser.write(self.char_in)
                             
                        else:
                            a = len(self.my_str)
                            b = a-1
                            self.my_str = self.my_str[:b]
                            self.ser.write(self.char_in)
                             
                    elif self.char_in == '\r' or self.char_in == '\n':
                         self.omega_share.write(int(self.my_str))
                         self.enc_flag.write(0)
                         self.state = s14
                         self.my_str = ''
                         self.Tref = utime.ticks_ms()
                         self.end_10 = utime.ticks_add(self.Tref, 10000)
                         self.end_start = utime.ticks_add(self.Tref,10)
                         print('Running Motor 2 for 10 Seconds')
                         self.mot_flag.write(5)
                      
                         
            elif self.state == s14:
                
                self.idx = 0
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 's' or self.char_in == 'S':
                        self.state = s15
                        self.duty_share.write(0)
                        self.mot_flag.write(1)
                        self.omega_share.write(0)
                if (utime.ticks_ms() >= self.end_start):
                    if self.delta2_share.read() == 0:
                        self.state = s15
                        self.duty_share.write(0)
                        self.mot_flag.write(1)
                        self.omega_share.write(0)
                        print('Fault detected, program stopped')
                if (utime.ticks_ms() >= self.end_10):
                    self.state = s15
                    self.duty_share.write(0)
                    self.mot_flag.write(1)
                    self.omega_share.write(0)
                if(utime.ticks_ms() >= self.next_time2):
                
                    self.delta_share.read()
                    self.my_list_pos.append(self.omegameas2_share.read())
                    self.my_list_time.append(utime.ticks_diff(utime.ticks_ms(),self.Tref))
                    self.my_list_actuation.append(self.duty_share.read())
                    self.next_time2= utime.ticks_add(utime.ticks_ms(),self.t_period)
                    
                    
            elif self.state == s15:
                for idx in range(len(self.my_list_pos)):
                            print('{:} {:} {:}'.format(self.my_list_pos[idx],
                                  self.my_list_time[idx]/1000,self.my_list_actuation[idx]))
                self.state = s1
                self.my_list_pos = []
                self.my_list_time = []  
                self.my_list_actuation = []
            else:
                raise ValueError('Invalid State')
                
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1
            

