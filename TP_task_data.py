"""
    @file           TP_task_data.py
    @brief          Task that collects and prints data to Putty.      
"""
import time

class TP_Task_Data:
    ''' @brief      Data Collection Task class
        @details    This task receives measured data via shared variables and 
                    sorts the data into arrays and prints them out into Putty.
    '''
    
    def __init__(self, period, x_share, y_share, Vx_share, Vy_share, theta_x_share, theta_y_share, omega_x_share, omega_y_share, dat_flag):
        ''' @brief                  Constructs a Data Collection Task 
            @details                The data collection task is implemented as a finite state machine. Once 
                                    indicated to do so, it will collect data and return it within Putty.
            @param period           The period, in milliseconds, between runs of the task.
            @param x_share          A shares.Share object reepresenting the x posiion of the ball
            @param y_share          A shares.Share object representing the y position of the ball
            @param Vx_share         A shares.Share object representing the x velocity of the ball
            @param Vy_share         A shares.Share object representing the y velocity of the ball
            @param theta_x_share    A shares.Share object representing the angular position
                                    position of the platform with respect to the x axis
            @param theta_y_share    A shares.Share object representing the angular position
                                    of the platform with respect to the y axis
            @param omega_x_share    A shares.Share object representing the angular velocity
                                    of the platfrom with respect to the x axis
            @param omega_y_share    A shares.Share object representing the angular velocity
            @param dat_flag         A shares.share object indicating when to begin data collection
        '''
        ## The period (in us) of the task
        self.period = period
        
        ## A shares.Share object that represents the x position of the ball
        self.x_share = x_share
        
        ## A shares.Share object that represents the y position of the ball
        self.y_share = y_share
        
        ## A shares.share object that represents the x velocity of the ball
        self.Vx_share = Vx_share
        
        ## A shares.share object that represents the y velocity of the ball
        self.Vy_share = Vy_share
        
        ## A shares.Share object that represents the angular position of the
        ## platform with respect to the x axis
        self.theta_x_share = theta_x_share
        
        ## A shares.Share object that represents the angular position of the
        ## platform with respect to the y axis
        self.theta_y_share = theta_y_share
        
        ## A shares.Share object that represents the angular velocity of the
        ## platform with respect to the x axis
        self.omega_x_share = omega_x_share
        
        ## A shares.Share object that represents the angular velocity of the
        ## platform with respect to the y axis
        self.omega_y_share = omega_y_share
        
        ## A shares.Share object that signals when to begin data collection
        self.dat_flag = dat_flag
        
        ## The time to run the next iteration of the task
        self.next_time = time.ticks_add(time.ticks_ms(), self.period)
        
        ## The number of runs of the state machine
        self.runs = 0
        
        ## The list of the x position [mm] of the ball from the center when data collection starts.
        self.list_x = []
        
        ## The list of the y position [mm] of the ball from the center when data collection starts.
        self.list_y = []
        
        ## The list of the angular position [deg] of the platform about the y axis when data collection starts.
        self.list_theta_x = []
        
        ## The list of the angular position [deg] of the platform about the x axis when data collection starts.
        self.list_theta_y = []
        
        ## The list of the x velocity [mm/s] of the ball from the center when data collection starts.
        self.list_Vx = []
        
        ## The list of the y velocity [mm/s] of the ball from the center when data collection starts.
        self.list_Vy = []
        
        ## The list of the angular velocity [deg/s] of the platform about the y axis when data collection starts.
        self.list_omega_x = []
        
        ## The list of the angular velocity [deg/s] of the platform about the x axis when data collection starts.
        self.list_omega_y = []
        
        ## The list of time [ms] when data collection starts.
        self.list_time = []

    def run(self):
        ''' @brief      Runs one iteration of the FSM
            @details    Collects data regarding the position and velocity of
                        ball and platform
        '''
        if(time.ticks_ms() >= self.next_time):
            if self.dat_flag.read() == 1:
                if self.runs == 0:
                    self.time_now = time.ticks_ms()
                self.time = time.ticks_diff(time.ticks_ms(), self.time_now)
                
                self.list_x.append(self.x_share.read())
                self.list_y.append(self.y_share.read())
                self.list_theta_x.append(self.theta_x_share.read())
                self.list_theta_y.append(self.theta_y_share.read())
                self.list_Vx.append(self.Vx_share.read())
                self.list_Vy.append(self.Vy_share.read())
                self.list_omega_x.append(self.omega_x_share.read())
                self.list_omega_y.append(self.omega_y_share.read())
                self.list_time.append(self.time)
        
                self.runs =+ 1
            if self.dat_flag.read() == 2:
                x = range(len(self.list_x))
                for idx in x:
                    print('{:},{:},{:},{:},{:},{:},{:},{:},{:}'.format(self.list_x[idx],self.list_y[idx],self.list_theta_x[idx],self.list_theta_y[idx],self.list_Vx[idx],self.list_Vy[idx],self.list_omega_x[idx],self.list_omega_y[idx],self.list_time[idx]))
                self.list_x = []
                self.list_y = []
                self.list_theta_x = []
                self.list_theta_y = []
                self.list_Vx = []
                self.list_Vy = []
                self.list_omega_x = []
                self.list_omega_y = []
                self.list_time = []
                self.dat_flag.write(0)
        
                
            
            if self.dat_flag.read() == 0:
                self.runs = 0
            
            self.next_time += self.period
    
        