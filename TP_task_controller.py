"""
    @file           TP_task_controller.py
    @brief          Task that implements closed-loop control. 
"""

import TP_closedloop
import time 

class TP_Task_Controller:
    ''' @brief      Controller Task class
        @details    This task receives measured data via shared variables and 
                    calculates the duty cycle of each motor using the Closed_loop driver.
                    
    '''

    def __init__(self, period, x_share, y_share, z_share, Vx_share, Vy_share, theta_x_share, theta_y_share, omega_x_share, omega_y_share, duty_1, duty_2, con_flag):
            ''' @brief                  Constructs a controller task
                @details                The controller task continuously updates the required
                                        needed by each motor to balance the ball using functions
                                        from the closed loop driver
                @param period           The period, in microseconds, between runs of the task.
                @param x_share          A shares.Share object reepresenting the x posiion of the ball
                @param y_share          A shares.Share object representing the y position of the ball
                @param z_share          A shares.Share object representing whether or not something is in contact with the plate
                @param Vx_share         A shares.Share object representing the x velocity of the ball
                @param Vy_share         A shares.Share object representing the y velocity of the ball
                @param theta_x_share    A shares.Share object representing the angular position
                                        position of the platform with respect to the x axis
                @param theta_y_share    A shares.Share object representing the angular position
                                        of the platform with respect to the y axis
                @param omega_x_share    A shares.Share object representing the angular velocity
                                        of the platfrom with respect to the x axis
                @param omega_y_share    A shares.Share object representing the angular velocity
                @param duty_1           A shares.Share object representing the duty cycle for motor 1
                @param duty_2           A shares.Share object representing the duty cycle for motor 2
                @param con_flag         A shares.share object indicating when to begin the controller task
            '''
            ## The period (in us) of the task
            self.period = period
            
            ## A shares.Share object that represents the x position of the ball
            self.x_share = x_share
            
            ## A shares.Share object that represents the y position of the ball
            self.y_share = y_share
            
            ## A shares.Share object that represent whether or not the ball is touching the plate
            self.z_share = z_share
            
            ## A shares.share object that represents the x velocity of the ball
            self.Vx_share = Vx_share
            
            ## A shares.share object that represents the y velocity of the ball
            self.Vy_share = Vy_share
            
            ## A shares.Share object that represents the angular position of the
            ## platform with respect to the y axis
            self.theta_x_share = theta_x_share
            
            ## A shares.Share object that represents the angular position of the
            ## platform with respect to the x axis
            self.theta_y_share = theta_y_share
            
            ## A shares.Share object that represents the angular velocity of the
            ## platform with respect to the y axis
            self.omega_x_share = omega_x_share
            
            ## A shares.Share object that represents the angular velocity of the
            ## platform with respect to the x axis
            self.omega_y_share = omega_y_share
            
            ## The desired values for the state vector 
            self.Reference_Vector = [0,0,0,0]
            
            ## A shares.Share object representing the duty cycle for motor 1
            self.duty_1 = duty_1
            
            ## A shares.Share object representing the duty cycle for motor 2
            self.duty_2 = duty_2
            
            ## A shares.Share object that indicates when to begin the controller task
            self.con_flag = con_flag
            
            ## A TP_closedloop.TP_ClosedLoop object that allows for use of closed loop functions on motor 1
            self.Controller_x = TP_closedloop.TP_ClosedLoop([-0.3, -10, -0.05, -2])
            
            ## A TP_closedloop.TP_ClosedLoop object that allows for use of closed loop functions on motor 2
            self.Controller_y = TP_closedloop.TP_ClosedLoop([-0.3, 10, -0.05, 2])
            
            ## The time to run the next iteration of the task
            self.next_time = time.ticks_add(time.ticks_us(), self.period)
            
            ## The number of runs of the state machine
            self.runs = 0
            
    def run(self):
        ''' @brief      Runs one iteration of the FSM
            @details    Continuously calculates the required duty cycle for each motor
                        using closed loop functions based on a given period.
        '''
        if(time.ticks_us() >= self.next_time):
            if self.con_flag.read() == 1:
            
                if self.z_share.read() == True:
                    self.Controller_x.set_K_Vector([-1, -5, -.1, -.03])
                    self.Controller_y.set_K_Vector([1.5, 5, .1, .1])
                    self.Measured_Vector_x = [self.x_share.read(),self.theta_x_share.read(),self.Vx_share.read(),self.omega_x_share.read()]
                    self.Measured_Vector_y = [self.y_share.read(),self.theta_y_share.read(),self.Vy_share.read(),self.omega_y_share.read()]                  
                    self.duty_1.write(self.Controller_x.update(self.Reference_Vector,self.Measured_Vector_x))
                    self.duty_2.write(self.Controller_y.update(self.Reference_Vector,self.Measured_Vector_y))

                    
                elif self.z_share.read() == False:
                    self.Controller_x.set_K_Vector([0, -7.5, 0, -.03])
                    self.Controller_y.set_K_Vector([0, 9.5, 0, .1])
                    self.Measured_Vector_x = [0 ,self.theta_x_share.read(),0 ,self.omega_x_share.read()]
                    self.Measured_Vector_y = [0 ,self.theta_y_share.read(),0 ,self.omega_y_share.read()]   
                    self.duty_1.write(self.Controller_x.update(self.Reference_Vector,self.Measured_Vector_x))
                    self.duty_2.write(self.Controller_y.update(self.Reference_Vector,self.Measured_Vector_y))
                    pass
                
            self.next_time += self.period
            self.runs += 1
                    
            


