"""
    @file           TP_task_motor.py
    @brief          Task that implements motor control.      
"""
import TP_motor
import time

class TP_Task_Motor:
    ''' @brief      Motor Task class
        @details    This task receives duty cycles from the closed loop task and 
                    controls both motors with the duty cycles.
    '''
    
    def __init__(self, period, duty_1, duty_2):
        ''' @brief          Constructs a motor task
            @details        The motor task continuously impliments the duty cycle that was
                            calculated in the controller task for each motor
            @param period   The period, in microseconds, between runs of the task
            @param duty_1   A shares.Share object representing the duty cycle for motor 1
            @param duty_2   A shares.Share object representing the duty cycle for motor 2
        '''
        ## The period (in us) of the task
        self.period = period
        
        ## A shares.Share object respresenting the duty cycle for motor 1
        self.duty_1 = duty_1
        
        ## A shares.Share object representing the duty cycle for motor 2
        self.duty_2 = duty_2
        
        ## A TP_motor.TP_Motor object that is used to run motor functions in the task
        self.Motor = TP_motor.TP_Motor()

        ## The time to run the next iteration of the task
        self.next_time = time.ticks_add(time.ticks_us(), self.period)
        
        ## The number of runs of the state machine
        self.runs = 0
        
    def run(self):
        ''' @brief      Runs one iteration of the FSM
            @details    Continuously impliments the duty cycle for each motor that
                        was calculated in the controller task
        '''
        if(time.ticks_us() >= self.next_time):
            ## sets the duty cycle for motor 1
            self.Motor.set_duty(1, self.duty_1.read())
            ## sets the duty cylce for motor 2
            self.Motor.set_duty(2, self.duty_2.read())
            
            self.next_time += self.period
            self.runs += 1
        
    

