"""
    @file           TP_main.py
    @brief          Main file that runs tasks cooperatively to balance a ball.      
"""
import TP_task_imu
import TP_task_touchpanel
import TP_task_data
import TP_task_controller
import TP_task_motor
import TP_task_user
import shares

def main():
    
    ## A shares.Share object that represent whether or not the ball is touching the plate
    z_share = shares.Share(0)
    
    ## A shares.Share object that represents the x position of the ball
    x_share = shares.Share(0)
    
    ## A shares.Share object that represents the y position of the ball
    y_share = shares.Share(0)
    
    ## A shares.share object that represents the x velocity of the ball
    Vx_share = shares.Share(0)
    
    ## A shares.share object that represents the y velocity of the ball
    Vy_share = shares.Share(0)
    
    ## A shares.Share object that represents the angular position of the
    ## platform with respect to the y axis
    theta_x_share = shares.Share(0)
    
    ## A shares.Share object that represents the angular position of the
    ## platform with respect to the x axis
    theta_y_share = shares.Share(0)
    
    ## A shares.Share object that represents the angular velocity of the
    ## platform with respect to the y axis
    omega_x_share = shares.Share(0)
    
    ## A shares.Share object that represents the angular velocity of the
    ## platform with respect to the x axis
    omega_y_share = shares.Share(0)
    
    ## A shares.Share object representing the duty cycle for motor 1
    duty_1 = shares.Share(0)
    
    ## A shares.Share object representing the duty cycle for motor 2
    duty_2 = shares.Share(0)
    
    ## A shares.Share object that indicates when to begin the controller task
    con_flag = shares.Share(0)
    
    ## A shares.Share object that indicates what state to run within the data collection FSM
    dat_flag = shares.Share(0)
    
    task_1 = TP_task_user.TP_Task_User(10_000, con_flag, dat_flag)
    
    task_2 = TP_task_touchpanel.TP_Task_TouchPanel(10_000, x_share, y_share, z_share, Vx_share, Vy_share)
    
    task_3 = TP_task_imu.TP_Task_IMU(10_000, theta_x_share, theta_y_share, omega_x_share, omega_y_share)
    
    task_4 = TP_task_controller.TP_Task_Controller(10_000, x_share, y_share, z_share, Vx_share, Vy_share, theta_x_share, theta_y_share, omega_x_share, omega_y_share, duty_1, duty_2, con_flag)
    
    task_5 = TP_task_motor.TP_Task_Motor(10_000, duty_1, duty_2)
    
    task_6 = TP_task_data.TP_Task_Data(50, x_share, y_share, Vx_share, Vy_share, theta_x_share, theta_y_share, omega_x_share, omega_y_share, dat_flag)

    task_list = [task_1, task_2, task_3, task_4, task_5, task_6]
    
    while(True):
        try: 
            for task in task_list:
                task.run()
        except KeyboardInterrupt:
            break
            print('Program Terminating')
    
if __name__ =='__main__':
   main()