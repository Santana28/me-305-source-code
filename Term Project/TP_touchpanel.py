"""
    @file           TP_touchpanel.py
    @brief          Driver class that implements touchpanel control.   
"""
import pyb


class TP_TouchPanel:
    ''' @brief      TouchPanel driver class
        @details    Objects of this class can be used to scan the x, y and z 
                    position of anything in contact with the touch panel.
    '''
    
    def __init__(self, Pin_xp, Pin_yp, Pin_xm, Pin_ym, length, width, x_c, y_c):
        ''' @brief          Initializes and returns a TouchPanel object.
            @param Pin_xp   The Pin setup for plus x node of the touchpanel.
            @param Pin_yp   The Pin setup for plus y node of the touchpanel.
            @param Pin_xm   The Pin setup for minus x node of the touchpanel.
            @param Pin_ym   The Pin setup for minus y node of the touchpanel.
            @param length   The length of the touchpanel[mm].
            @param width    The width of the touchpanel[mm].
            @param x_c      The x-coordinate of the center of the touchpanel[mm].
            @param y_c      The x-coordinate of the center of the touchpanel[mm].
            
        '''
        
        ##The Pin setup for the plus x node of the touch panel
        self.Pin_xp = Pin_xp
        
        ##The pin setup for the plus y node of the touch panel
        self.Pin_yp = Pin_yp
        
        ##The Pin setup for the minus x node of the touch panel
        self.Pin_xm = Pin_xm
        
        ##The pin setup for the minus y node of the touch panel
        self.Pin_ym = Pin_ym
        
        ##The width of the touch panel
        self.width = width
        
        ##The length of the touch panel
        self.length = length
        
        ##The x-coordinate of the center of the touch panel
        self.x_c = x_c
        
        ##The y-coordinate of the center of the touch panel
        self.y_c = y_c
 
    
    def scan_xyz(self, coordinate):
        ''' @brief              Scans the x, y or z position of the object in contact with the touchpanel.
            @param coordinate   The specified coordinate to scan (x, y or z).
        '''
        if coordinate == 1:
            self.x_p = pyb.Pin(self.Pin_xp, pyb.Pin.OUT_PP)
            self.x_p.high()
            self.x_m = pyb.Pin(self.Pin_xm, pyb.Pin.OUT_PP)
            self.x_m.low()
            self.y_p = pyb.Pin(self.Pin_yp, pyb.Pin.IN)
            self.y_m = pyb.Pin(self.Pin_ym, pyb.Pin.IN)
            self.AD_ym = pyb.ADC(self.Pin_ym)
            self.xpos = self.AD_ym.read()*(self.length/4095)-self.x_c
            return self.xpos
        
        if coordinate == 2:
            self.y_p = pyb.Pin(self.Pin_yp, pyb.Pin.OUT_PP)
            self.Pin_yp.high()
            self.y_m = pyb.Pin(self.Pin_ym, pyb.Pin.OUT_PP)
            self.Pin_ym.low()
            self.x_p = pyb.Pin(self.Pin_xp, pyb.Pin.IN)
            self.x_m = pyb.Pin(self.Pin_xm, pyb.Pin.IN)
            self.AD_xm = pyb.ADC(self.Pin_xm)
            self.ypos = self.AD_xm.read()*(self.width/4095)-self.y_c
            return self.ypos
        
        if coordinate == 3:
            self.y_p = pyb.Pin(self.Pin_yp, pyb.Pin.OUT_PP)
            self.Pin_yp.high()
            self.x_m = pyb.Pin(self.Pin_xm, pyb.Pin.OUT_PP)
            self.Pin_xm.low()
            self.x_p = pyb.Pin(self.Pin_xp, pyb.Pin.IN)
            self.y_m = pyb.Pin(self.Pin_ym, pyb.Pin.IN)
            self.AD_z = pyb.ADC(pyb.Pin.cpu.A0)
            self.zpos = self.AD_z.read()
            if self.zpos < 3900:
                self.z = True
            else:
                self.z = False
            return self.z
                

    
   

        

 
