"""
    @file           TP_motor.py
    @brief          Driver class that sets up and controls motors.       
"""

import pyb

class TP_Motor:
    ''' @brief      A motor class for one channel of the DRV8847
        @details    Objects of this class can be used to apply PWM to a
                    given DC motor
    '''
    
    def __init__(self):
        ''' @brief      Initializes and returns a motor object associatted with the DRV8847
            @details    Initializes all the pins and timers associated with the motor
               
        '''
        ## Initializes timer 3
        self.tim3 = pyb.Timer(3, freq = 20000)
        ## Initializes pin B4
        self.pinIN1 = pyb.Pin(pyb.Pin.cpu.B4)
        ## Initializes pin B5
        self.pinIN2 = pyb.Pin(pyb.Pin.cpu.B5)
        ## Initializes pin B0
        self.pinIN3 = pyb.Pin(pyb.Pin.cpu.B0)
        ## Initializes pin B1
        self.pinIN4 = pyb.Pin(pyb.Pin.cpu.B1)
        ## Configures channel 1 for timer 3
        self.t3ch1 = self.tim3.channel(1, pyb.Timer.PWM, pin=self.pinIN1)
        ## Configures channel 2 for timer 3
        self.t3ch2 = self.tim3.channel(2, pyb.Timer.PWM, pin=self.pinIN2)
        ## Configures channel 3 for timer 3
        self.t3ch3 = self.tim3.channel(3, pyb.Timer.PWM, pin=self.pinIN3)
        ## Configures channel 4 for timer 3
        self.t3ch4 = self.tim3.channel(4, pyb.Timer.PWM, pin=self.pinIN4)

        pass
    
    def disable(self, motor):
        ''' @brief          Disables selected motor.
            @param motor    Indicates which motor is selected.
            
        '''
        # self.motor = motor
        
        if motor == 1:
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(0)
        pass
    
        if motor == 2:
                self.t3ch3.pulse_width_percent(0)
                self.t3ch4.pulse_width_percent(0)
        pass
        
    def set_duty(self, motor, duty):
        ''' @brief      Sets the PWm duty cylce for the motor channel
            @details    This method sets the duty cycle to be sent to the 
                        motor to the given level. posotive values cause effort
                        in one dierction, negative values in the opposite
                        direction
            @param      duty    A signed number holding the duty cycle
                                of the PWM signal sent to the motor
        '''
        # self.motor = motor
        
        if motor == 1:
            if duty > 0:
                self.t3ch1.pulse_width_percent(100)
                self.t3ch2.pulse_width_percent(100-abs(duty))
            elif duty < 0:
                self.t3ch1.pulse_width_percent(100-abs(duty))
                self.t3ch2.pulse_width_percent(100)
            pass
        if motor == 2:
            if duty > 0:
                self.t3ch3.pulse_width_percent(100)
                self.t3ch4.pulse_width_percent(100-abs(duty))
            elif duty < 0:
                self.t3ch3.pulse_width_percent(100-abs(duty))
                self.t3ch4.pulse_width_percent(100)
            pass
    



        
    
    
    
    
    
    
    
    

